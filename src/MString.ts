export class MString {
    constructor(inputString: string) {
        this.setString(inputString);
    }

    public getString(): string {
        return this.contents.join("");
    }

    public setString(inputString: string): void {
        this.contents = [];

        this.pushString(inputString);
    }

    public pushString(inputString: string): void {
        for (let i = 0; i < inputString.length; i++) {
            this.contents.push(inputString[i]);
        }
    }

    public popString(startIndex: number, numElements: number): string {
        return this.contents.splice(startIndex, numElements).join("");
    }

    public getChar(index: number): string {
        return this.contents[index];
    }

    public setChar(index: number, character: string): void {
        this.contents[index] = character;
    }

    public pushChar(character: string): void {
        this.contents[this.length()] = character;
    }

    public popChar(index: number): string {
        return this.contents.splice(index, 1)[0];
    }

    public length(): number {
        return this.contents.length;
    }

    public find(substring: string): number {
        return this.getString().indexOf(substring);
    }

    private contents: string[] = [];
}

export default MString;

