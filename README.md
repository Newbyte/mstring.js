# MutableString.js

MutableString.js is an implementation of mutable strings for JavaScript, written
 in TypeScript.

## Installation

MutableString.js is available on npm and can be installed via the following
 command:

`npm install @newbytee/mstring`

## Example code

```typescript
// Declare a new MutableString
let foo = new MString("greetings!");

// Replace the "!" with a question mark
foo.setChar(foo.find("!"), "?");

// Log the result as a string
console.log(foo.getString());
```

    
