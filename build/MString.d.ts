export declare class MString {
    constructor(inputString: string);
    getString(): string;
    setString(inputString: string): void;
    pushString(inputString: string): void;
    popString(startIndex: number, numElements: number): string;
    getChar(index: number): string;
    setChar(index: number, character: string): void;
    pushChar(character: string): void;
    popChar(index: number): string;
    length(): number;
    find(substring: string): number;
    private contents;
}
export default MString;
