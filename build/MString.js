"use strict";
exports.__esModule = true;
exports.MString = void 0;
var MString = (function () {
    function MString(inputString) {
        this.contents = [];
        this.setString(inputString);
    }
    MString.prototype.getString = function () {
        return this.contents.join("");
    };
    MString.prototype.setString = function (inputString) {
        this.contents = [];
        this.pushString(inputString);
    };
    MString.prototype.pushString = function (inputString) {
        for (var i = 0; i < inputString.length; i++) {
            this.contents.push(inputString[i]);
        }
    };
    MString.prototype.popString = function (startIndex, numElements) {
        return this.contents.splice(startIndex, numElements).join("");
    };
    MString.prototype.getChar = function (index) {
        return this.contents[index];
    };
    MString.prototype.setChar = function (index, character) {
        this.contents[index] = character;
    };
    MString.prototype.pushChar = function (character) {
        this.contents[this.length()] = character;
    };
    MString.prototype.popChar = function (index) {
        return this.contents.splice(index, 1)[0];
    };
    MString.prototype.length = function () {
        return this.contents.length;
    };
    MString.prototype.find = function (substring) {
        return this.getString().indexOf(substring);
    };
    return MString;
}());
exports.MString = MString;
exports["default"] = MString;
//# sourceMappingURL=MString.js.map