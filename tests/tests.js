const assert = require("assert");
const MString = require("../build/MString.js");

const mstring = new MString.MString("hello");

assert.equal("hello", mstring.getString());

mstring.setString("moop");

assert.equal("moop", mstring.getString());

mstring.pushString("er");

assert.equal("mooper", mstring.getString());

assert.equal("oope", mstring.popString(1, 4));

assert.equal("mr", mstring.getString());

assert.equal("m", mstring.getChar(0));

mstring.setChar(1, "o");

assert.equal("mo", mstring.getString());

mstring.pushChar("z");

assert.equal("moz", mstring.getString());

assert.equal(mstring.popChar(0), "m");

assert.equal(mstring.length(), 2);

assert.equal(mstring.find("oz"), 0);

console.log(" - Tests finished successfully! No errors.\n");

